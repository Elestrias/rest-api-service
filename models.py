from main import db


class Courier(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    courier_type = db.Column(db.String, primary_key=True)
    regions = db.Column(db.String, primary_key=True)
    working_hours = db.Column(db.String, primary_key=True)
    rating = db.Column(db.Float)
    earnings = db.Column(db.Integer)


class Order(db.Model):
    order_id = db.Column(db.Integer, primary_key=True)
    weight = db.Column(db.Float, primary_key=True)
    region = db.Column(db.Integer, primary_key=True)
    delivery_hours = db.Column(db.String, primary_key=True)
