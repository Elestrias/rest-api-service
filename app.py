from flask import Flask, request, jsonify, render_template, abort, Response
from main import app
from extra import *


@app.route('/couriers', methods=["POST"])
def couriers():
    try:
        data = request.get_json()["data"]
    except Exception:
        return Response(status=400)
    print(data)
    good_list = []
    bad_list = []
    response = 200
    for items in data:
        flag = False
        try:
            courier_id = items["courier_id"]
            courier_type = items["courier_type"]
            regions = items["regions"]
            working_hours = items["working_hours"]

            flag = True
        except Exception:
            print(items)
            bad_list.append(items)
            response = 400
        try:
            rating = items["rating"]
        except Exception:
            rating = 0.0
        if flag:
            create_courier(courier_id,
                         courier_type,
                         regions,
                         working_hours)

    if response == 400:
        list  = []
        for i in range(len(bad_list)):
            print(bad_list)
            list.append({"id": int(bad_list[i]["courier_id"])})
            content = jsonify({"validation_error": {"couriers": list}})
        return content, 400
    elif response == 200:
        list = []
        for items in data:
            list.append({"id": items["courier_id"]})
            content = jsonify({"couriers": list})
        return content, response



def add_couriers(courier_id):
    return "ppspsps"

def patch_couriers():
    return "ppsss"


def post_orders():
    return 'ssssdddd'


def assign_order():
    return 'ddwdsd'

def complete_order():
    return None


app.run(host='127.0.0.1', port=5000, debug=True)