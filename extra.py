from main import db
from models import *


def is_courier_exists(courier_id):
    return Courier.query.filter_by(id=courier_id).first() is not None


def create_courier(courier_id, courier_type, regions, working_hours, earnings=0.0, rating=0.0):
    working_hours = "  ".join(working_hours)
    regions = "  ".join(list(map(lambda x: str(x), regions)))
    user = Courier(id=courier_id,
                   courier_type=courier_type,
                   regions=regions,
                   working_hours=working_hours,
                   rating=rating,
                   earnings=earnings)
    db.session.add(user)